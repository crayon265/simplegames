package main.java;

import javax.swing.*;
import java.awt.*;

public class Snake extends JFrame {
    public Snake(){
        setResizable(false);
        setTitle("Snake game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        add(new Board(this));
        pack();
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(()->{
            JFrame w = new Snake();
            w.setVisible(true);
        });
    }


}
