package main.java;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Dimension2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Timer;


public class Board extends JPanel implements ActionListener {
    private final int SQUARE_SIZE = 10;
    private List<Integer> x = new ArrayList<Integer>();
    private List<Integer> y = new ArrayList<Integer>();
    private int destroyedTailNumber
            = -1;
    private int snakeSize;
    private int apple_x;
    private int apple_y;
    private boolean leftDirection = false;
    private boolean rightDirection = true;
    private boolean upDirection = false;
    private boolean downDirection = false;
    private Timer timer;
    private boolean inGame = true;
    private Image apple;
    private Image head;
    private Image chest;
    private int scoreNumber;
    private boolean onPause;
    private JFrame snake;
    private double screenWidth;
    private double screenHeight;
        public Board(JFrame snake){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        screenWidth = screenSize.getWidth();
        screenHeight = screenSize.getHeight();
        this.snake = snake;
        addKeyListener(new TAdapter());
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (inGame == false) {
                    if (e.getX() > 257 && e.getX() < 400 && e.getY() > 0 && e.getY() < 27) {
                        initGame();
                    }
                }
                //e.getX()
                //e.getY()
            }
        });
        setFocusable(true);
        setBackground(Color.cyan);
        setPreferredSize(new Dimension(400,400));
        initGame();
    }

    private void initGame(){
        onPause = false;
        snakeSize = 3;
        scoreNumber = 0;
        x = new ArrayList<Integer>();
        y = new ArrayList<Integer>();
        for(int i = 0; i < snakeSize; i++){
            x.add(50 - i * 10);
            y.add(50);
        }
        createApple();
        ImageIcon iid = new ImageIcon("src/main/resources/apple.png");
        ImageIcon iid1 = new ImageIcon("src/main/resources/квадрат.png");
        ImageIcon iid2 = new ImageIcon("src/main/resources/белый_квадрат.png");
        chest = iid2.getImage();
        head = iid1.getImage();
        apple = iid.getImage();
        if(timer == null){
            timer = new Timer(150,this);
            timer.start();
        }
        else{
            timer.setDelay(150);
        }
        leftDirection = false;
        rightDirection = true;
        upDirection = false;
        downDirection = false;
        inGame = true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (inGame == true) {
            repaint();
            if (onPause == false) {
                check();
                System.out.println(inGame);
                if(inGame == true){
                    move();
                }
            }
        }
    }
    private void check() {
        if (x.get(0) == apple_x && y.get(0) == apple_y) {
            createApple();
            x.add(x.get(snakeSize - 1));
            y.add(y.get(snakeSize - 1));
            snakeSize += 1;
            scoreNumber += 10;
            int oldDelay = timer.getDelay();
            timer.setDelay(oldDelay - (int) Math.log(scoreNumber));
        }
        //if(x.get(0) > 390 || y.get(0) > 390 || x.get(0) < 0 || y.get(0) < 30){
        if (rightDirection == true) {
            if (x.get(0) + 10 > 390) {
                inGame = false;
            }
        }
        if (downDirection == true) {
            if (y.get(0) + 10 > 390) {
                inGame = false;
            }
        }
        if (leftDirection == true) {
            if (x.get(0) + 10 < 20) {
                inGame = false;
            }
        }
        if (upDirection == true) {
            if (y.get(0) + 10 < 50) {
                inGame = false;
            }
        }

        //System.out.println(x.toString());
        //System.out.println(y.toString());

        for (int z = 1; z < snakeSize; z++) {
            //if(x.get(0).equals(x.get(z)) && y.get(0).equals(y.get(z))){
            if (rightDirection == true) {
                if (x.get(0) + 10 == (x.get(z)) && y.get(0).equals(y.get(z))) {
                    destroyedTailNumber = z;
                    inGame = false;
                }
            }
            if (downDirection == true) {
                if (y.get(0) + 10 == (y.get(z)) && x.get(0).equals(x.get(z))) {
                    destroyedTailNumber = z;
                    inGame = false;
                }
            }
            if (leftDirection == true) {
                if (x.get(0) - 10 == (x.get(z)) && y.get(0).equals(y.get(z))) {
                    destroyedTailNumber = z;
                    inGame = false;
                }
            }
            if (upDirection == true) {
                if (y.get(0) - 10 == (y.get(z)) && x.get(0).equals(x.get(z))) {
                    destroyedTailNumber = z;
                    inGame = false;
                }
            }
        }
    }
        //if(inGame == false) {
            //timer.stop();
        //}
    private void move(){
        for(int z = snakeSize-1;z > 0; z--){
            x.set(z,x.get(z-1));
            y.set(z,y.get(z-1));
        }
        if(leftDirection == true){
            x.set(0,x.get(0)-SQUARE_SIZE);
        }
        if(rightDirection == true){
            x.set(0,x.get(0)+SQUARE_SIZE);
        }
        if(upDirection == true){
            y.set(0,y.get(0)-SQUARE_SIZE);
        }
        if(downDirection == true){
            y.set(0,y.get(0)+SQUARE_SIZE);
        }
    }
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Font font = new Font("TimesRoman",Font.BOLD, 30);
        g.setFont(font);
        g.drawString("Scores: " + scoreNumber, 20, 27);
        g.drawImage(apple, apple_x, apple_y, this);
        g.drawLine(0, 30, 400,30);
        g.drawImage(head, x.get(0), y.get(0), this);
        for(int z = 1; z < snakeSize; z++){
            if(destroyedTailNumber == z) {
                g.drawImage(chest, x.get(z), y.get(z), this);
            }
            else {
                g.drawImage(apple, x.get(z), y.get(z), this);

            }
        }
        if(onPause == true) {
            g.drawString("PAUSE", 150, 200);
        }
        if(inGame == false) {
            g.drawString("YOU LOSE", 120, 200);
        }
        if(inGame == false) {
            g.drawString("RESTART", 257,27 );
        }
        Toolkit.getDefaultToolkit().sync();
    }
    private class TAdapter extends KeyAdapter{
        @Override
        public void keyPressed(KeyEvent e){
                int key = e.getKeyCode();
            if(key == KeyEvent.VK_SPACE ){
                if(onPause == false){
                    onPause = true;
                }
                else{
                    onPause = false;
                }
            }
            if(key == KeyEvent.VK_ESCAPE){
                snake.dispose();
            }
            //if(key == KeyEvent.VK_ESCAPE && inGame == false){
                //initGame();
            //}
            if(key == KeyEvent.VK_LEFT && rightDirection != true){
                leftDirection = true;
                rightDirection = false;
                upDirection = false;
                downDirection = false;
            }
            if(key == KeyEvent.VK_RIGHT && leftDirection != true){
                rightDirection = true;
                leftDirection = false;
                upDirection = false;
                downDirection = false;
            }
            if(key == KeyEvent.VK_UP && downDirection != true){
                upDirection = true;
                rightDirection = false;
                leftDirection = false;
                downDirection = false;
            }
            if(key == KeyEvent.VK_DOWN && upDirection != true){
                downDirection = true;
                leftDirection = false;
                rightDirection = false;
                upDirection = false;
            }
        }
    }
    private void createApple(){
        apple_x = (int) (Math.random() * 100) % 40 * 10;
        apple_y = 30 + (int) (Math.random() * 100) % 37 * 10;
        for(int i = 0; i < snakeSize; i++){
            if (x.get(i) == apple_x && y.get(i) == apple_y){
                createApple();
            }
        }
    }
}
//баг, яблоко не появилось на поле.
// д/з: 1- раскарсить голову змейки в черный цвет(паинт)
// 2 - запретить идти вниз, когда ты идешь вверх.(аналогично для остальных направлений);


//задача 1 - добавить напдись you lose при проигрише.
//задача 2 - исправить баг, из за коготорого змейка отрисовывается поверх надписи паузы.(все дело в очерёдности отрисовки).
// задача 3 - при проигрише добавить кнопку restart().
// сделать так, что бы координаты надписей(объектов) определялись не константами, а в зависимости от размеров окна.

//Д/З сделать рестарт при нажатии клавиши ESQ;

//Сделать так, что бы яблоко не спавнилось в том месте, где находиться тело змейки.